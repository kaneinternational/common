## EN 50379 REGULATED INSTRUCTIONS

EN 50379 Section 4.3.2 “Instructions” defines a number of specific points that must be included in the relevant instruction manuals. The paragraph numbering below relates to that section of EN 50379.

**a)** The {{ book.product.name }} is compliant the EN 50379 Part 2 and Part 3.

**b)** The {{ book.product.name }} is intended to be used with the following fuels:

- Natural gas
- Light oil (28/35 sec)
- Propane
- LPG
- Wood pellets
- Butane

**c)** The {{ book.product.name }} is designed for use with either non-rechargeable alkaline AA cells or rechargeable NiMH AA cells. Four cells are needed. Types cannot be
mixed. Under no circumstances should any attempt be made to recharge alkaline cells.

The battery charger supplied with the {{ book.product.name }} is rated for indoor use only. Its voltage input must be in the range 100 – 240 V ac at 50 – 60 Hz with a current capability of 0.3 A. The chargers output voltage is 9 V dc at a maximum of 0.66A.

The charger has no user serviceable components.

Only a correctly specified and rated charger must be used with the {{ book.product.name }}

**d)** The {{ book.product.name }} is not designed for continuous use and is not suitable for use as a fixed safety alarm.

**e)** An explanation of all the symbols used on the analyser’s display is given in Appendix 1 of this manual.

**f)** The recommended minimum time required to perform one complete measurement cycle and achieve correct indication of the measured values in EN 50379 Part 2 is 110 seconds. This is based on the T 90 times defined in the standard, always assuming that parameters being measured have reached stability. This time is the summation of the times for a draught test (10 secs) and a combustion test (90 secs) plus the time to move the hose connection from the pressure input to the water trap (10 secs)

The recommended minimum time required to perform one checking procedure in EN 50379 Part 3 is 110 seconds as described in section f) above.

**h)** Some commonly occurring materials, vapour or gases may affect the operation of the {{ book.product.name }} in the long or the short term though in normal use Kane International Ltd is not aware of any specific issues that have affected the product. The following list is included to satisfy the stated requirements of EN 50379:

- Solvents
- Cleaning fluids
- Polishes
- Paints
- Petrochemicals
- Corrosive gases

**i)** The {{ book.product.name }} is fitted with an electrochemical CO sensor and an infra-red CO<sub>2</sub> sensor which have an expected life of more than 5 years. The calibration of these sensors must be confirmed on an annual basis.

The batteries have an expected operational life of more than 500 re-charge cycles.

**j)** The {{ book.product.name }} is designed to operate at ambient temperatures in the range 0&deg;C to +45&deg;C with relative humidity of 10% to 90% non-condensing. Whilst it is recommended that the analyser is given the protection of a carry case during transportation it is not required for normal operation.

**k)** The {{ book.product.name }} has an initial start up delay following switch on of between 90 and 60 seconds dependent on ambient temperature. There is no additional delay after battery replacement.

**l)** Most sensors used in combustion analysers give a zero output when they fail and it is widely recommended that analysers are regularly checked (also known as a bump test) using either a can of test gas or a known source of combustion products.

The {{ book.product.name }} must have its calibration checked on an annual basis and be issued with a traceable Certificate of Calibration.

The sensor within the {{ book.product.name }} can only be replaced by Kane International Ltd or one of its trained and approved service partners.

The water trap should be checked on a regular basis whilst the analyser is in use (every few minutes) as the amount of condensate generated varies with the fuel type, atmospheric conditions and the appliances operating characteristics.

The particle filter should be checked at least on a daily basis when using ‘clean’ fuels and more often when using liquid or solid fuels.

Detailed instructions regarding the changing of the filter and the emptying of the water trap are given in Section 2  of this manual.

**m) WARNING!** When using a {{ book.product.name }} to test an appliance a full visual inspection of the appliance, in accordance with its manufacturer’s instructions, must also be carried out.
