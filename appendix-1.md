## Appendix 1 - Main Parameter:

Here are the legends used and what they mean:

|    |                                                                                                                                                                                                                                |
| ---- | ----                                                                                                                                                                                                                           |
| O2   | Oxygen (Calculated) reading in percentage (%)                                                                                                                                                                                  |
| CO   | Carbon monoxide (Measured) reading displayed in ppm (parts per million)<br>If `- - - -` is displayed there is a fault with the CO sensor or the instrument has not set to zero correctly. Switch off instrument and try again. |
| COn | carbon monoxide normalised |
| CO2 | Carbon dioxide (Measured) reading in percentage (%) |
| Ra  | CO to CO2 ratio |
| Tf  | Temperature measured by the flue gas probe in centigrade (oC). It displays `- OC -` if the flue probe is disconnected or faulty. |
| Ti |  If an inlet temperature probe (optional) is connected into the T2 socket during its countdown, the measured temperature from the inlet probe will be used as the inlet temperature. If an inlet temperature probe is not connected to the analyser during countdown the measured temperature from the flue probe will be used as the inlet temperature.<br>If neither probe is connected during countdown the analyser’s internal ambient temperature will be used as the inlet temperature. |
| ΔT | Nett temperature calculated by deducting the INLET temperature from the measured FLUE temperature.  It displays ‘- OC -’ if the flue probe is not connected or broken. |
| EFF | Combustion efficiency calculation displayed in percentage either as Gross Ef(G) or Nett Ef(N) or Condensing Nett Ef(C) -  Use MENU to change. The calculation is determined by fuel type and uses the calculation in British Standard BS845. The efficiency is displayed during a combustion test, `- - - -` is displayed while in fresh air. |
| LOSS | Losses calculated from oxygen and type of fuel.  Displays reading during a combustion test. `- - - -` is displayed while in fresh air. |
| X&nbsp;-&nbsp;AIR | Excess air calculated from the calculated oxygen and type of fuel. Displays reading during a combustion test.  `- - - -` is displayed while in fresh air. |
| CO/CO2 | CO/CO2 Ratio: measured CO (ppm) divided by (CO2 (%) x 10,000). |
| PRS | Pressure reading, either single point or differential. |
| BAT | Displays the Battery power available. Readings may be affected if used with low power batteries. |
| DATE | Date shown as day, month and year, DD/MM/YY.  Date is recorded when each combustion test is printed or stored. |
| TIME | The time shown is expressed in “Military” time HH:MM:SS. Time is recorded when each test is printed or stored.<br><br>**Note! When changing the batteries on the instrument the memory will store the date and time for up to one minute, if outside this time it may be necessary to re-enter the details.<br>Date and time may also need to be reset if re-chargeable batteries are allowed to totally discharge.** |
| FULL | The maximum number of tests have been stored in the memory. To delete the stored memory, Select Reports then select the tests to be deleted. |

**Pressure units:**

|      |          |
| ---- | ----     |
| m    | millibar |
| s    | psi      |
| h    | hPa      |
| P    | Pa       |
| g    | mmHg     |
| i    | inH2O    |
| w    | mmH20    |
| k    | kPa      |

**SYMBOLS** used on the display

|           |                                                               |
| ----      | ----                                                          |
| PRS       | Pressure                                                      |
| Ra        | CO/CO2                                                        |
| XAIR      | Excess Air                                                    |
| Tf        | Flue temperature                                              |
| Ta        | Inlet temperature                                             |
| ∆T        | Nett temperature / Differential temperature                   |
| EfG       | Gross efficiency                                              |
| EfCG      | Gross condensing Efficiency                                   |
| EfN       | Nett efficiency                                               |
| EfCN      | Nett condensing efficiency                                    |
| -P-OFF    | Pump off                                                      |
| ‘O2++%    | Calculated oxygen greater than 18% so calculation is disabled |
| N/F       | Temperature input not fitted                                  |
| CAL       | Number of days left before recalibration is due               |
| BAT       | Battery level symbol                                          |
| N/F       | Not fitted.                                                   |
| INT       | Interval in seconds                                           |
| ppm       | parts per million                                             |
| p         | parts per million                                             |
| ppm(n)    | parts per million normalised                                  |
| O2ref     | reference level in % for normalisation calculation            |
| mg/m3     | milligrams per metre cubed                                    |
| mg/m3(n)  | milligrams per metre cubed normalised                         |
| mg/kWh    | milligrams per kilowatt hour                                  |
| mg/kWh(n) | milligrams per kilowatt hour normalised                       |
