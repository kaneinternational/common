COLD WEATHER PRECAUTIONS

It is important you keep your flue gas analyser in a warm place overnight Electronic devices that become really cold, by being left in a vehicle overnight, suffer when taken into a warm room the next morning.

Condensation may form which can affect the analyser�s performance & cause permanent damage Electrochemical sensors used in flue gas analysers can be affected by condensation or water being sucked into the analyser, as the small apertures on top of sensors can become blocked with water, stopping sensors seeing flue gas. When this happens, oxygen or carbon dioxide reading will display as "�" & sensors may be permanently damaged.

If you think that your analyser is affected by condensation or water ingress, it may be possible to rectify the problem yourself. Simple leave the analyser running in a warm place, with the pump 'ON' sampling fresh air for a few hours (use mains adapter/battery charger if needed). If, after doing this, you still experience  problems please contact our Service Centres.



