## ANALYSER ANNUAL SERVICE & RECERTIFY

Although sensor life is typically more than five years, the analyser should be serviced and re-certified annually to counter any long-term sensor or electronics drift or accidental damage.

Local regulations may require more frequent re-certification.

Kane International has service facilities at Atherton near Manchester Tel: 01942-873434 (the primary service centre for UK customers) and at Welwyn Garden  City in Hertfordshire Tel: 01707 375550 (the primary service centre for non-UK customers).

By sending your analyser back to Kane for an annual fixed price service (check www.kane.co.uk for details) you have the opportunity to extend the warranty on your analyser to 5 years.

### SERVICE - CALIBRATE - RECERTIFY

YOU CAN COUNT THE REASONS WHY ON ONE HAND!

1. Fixed prices ensure you know the full cost of ownership before you buy.

2. Flue Gas Analysers will drift out of calibration over time – only Kane can adjust them back to the manufacturers' specification & return them as accurate as when they were purchased.

3. Analysers always seem to require a service when you need them most! Kane's two UK Service Centres pride themselves on their fast service turnaround times.

4. Every Kane analyser is returned with a fully traceable calibration certificate & another 12 months warranty. After 6 years, warranty becomes limited to replacement parts only.

5. In some cases an out-of-calibration analyser means any combustion reports you produce are invalid, so it is very important you get your analyser recertified 

**KANE is now the UK's largest, fastest & most reliable FGA after sales service provider**

Our Northern & Southern Service Centres are the UK's only locations authorized  to Service - Calibrate - Recertify & Repair Kane analysers

You can organise your Kane analyser’s Service & Recertification online via your Dashboard on www.kane.co.uk, send it or call us to arrange a pre-booked while-you-wait service, saving you time & potential lost revenue

We pride ourselves on our fast turnaround for a fixed price, which includes:

- Inspecting your analyser & accessories, testing its functionality & adjusting it to our original specification

- Free software upgrades, where applicable

- Parts & labour on these replaceable parts as required: CO & CO<sub>2</sub>/O<sub>2</sub> sensors, pump & battery

- Parts not included are optionally fitted toxic cells, cases & probes

- All internal tubing, water trap, particle & chemical filters

- Recertifying to national standards & issuing a fully traceable calibration certificate to confirm your analyser's performance

- A complimentary Service Pack comprising PF400/5 (pack of five) replacement filter elements, Thermal Printer Roll, Water Trap Plug & Probe Connector (RRP for filters & roll = £17.40)

- Return carriage via Next Day delivery service (UK mainland only)

Kane is the only UK manufacturer of hand-held gas analysers granted UKAS accreditation to ISO / IEC 17025:2005 for its Welwyn Garden City calibration laboratory, covering a range of gases, temperature & pressure.  Kane is certified to ISO 9001, ISO 14001 & BS OHSAS 18001

Our Northern Service Centre offers service & repair for your other instruments, including: thermometers, pressure meters & multimeters 

### RETURNING YOUR ANALYSER TO KANE

Before returning your analyser to Kane, please ensure that you enclose:

- RMA label if you have used our new simple online booking in process
- Your full contact details
- A daytime telephone number
- Details of faults you might have experienced
- Any relevant accessories (eg. probe, printer, adaptor & leak detectors). Any accessories that are returned will be checked

#### PACKING YOUR ANALYSER 

When returning an analyser with its probe, please send them back in their carry bag. The bag should be put into a suitably sized box (approx. 45cm x 20cm x
23cm).

When only returning an analyser, use a container as big as a shoe box & pack out empty space with newspaper.

Before sealing your package, please ensure you followed our procedure above &amp; have clearly marked your box for the Kane Service Team. 

If you do not have an account with a courier company,  take your package to your local Post Office – we recommend Special Delivery so that it is insured & traceable in transit

#### WHEN WE RECEIVE YOUR ANALYSER

Our Service Engineers will inspect the analyser & accessories. If you haven't booked in & paid online, they will confirm the total service cost.  

Once accepted, the work will be carried out & on completion, returned to you by Next Day delivery service (UK mainland only).

#### WHERE TO SEND YOUR ANALYSER

**Northern Customer Service**  
Kane International Ltd  
Gibfield Park Avenue  
Atherton  
Manchester  
M46 0SY, UK  
t: +44 (0) 1942 873434  
f: +44 (0) 1942 873558  
e: nservice@kane.co.uk  

**Southern & International  Customer Service**  
Kane International Ltd  
Kane House, 11 Bessemer Road   
Welwyn Garden City  
Hertfordshire  
AL7 1GF, UK  
t: +44 (0) 1707 384834  
f: +44 (0) 1707 384833  
e: sservice@kane.co.uk    

