## END OF LIFE DISPOSAL

The Waste Electrical or Electronic Equipment (WEEE) Directive requires countries in the EU to maximise collection and environrmentally responsible processing of these items.

Products are now labelled with a crossed out wheeled bin symbol to remind you that they can be recycled.

**Please Note:**  Batteries used in this instrument should be disposed of in accordance with current legislation and local guidelines.
